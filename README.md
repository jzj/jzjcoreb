# JZJCoreB

Second RV32IZifencei soft core cpu implementation with higher performance.

I've learned quite a bit from JZJCoreA and some online CE lectures I've found on youtube. This design will be of a similar to style as JZJCoreA with some borrowed code, but with better performance so not single cycle or pipelined... yet. A goal is to make the memory mapping for this core compatible with JZJCoraA assembly I've written to make things easier to test, but I'll deviate if necessary.

I know I should be using git branches for big changes to files (instead of the awful fileNamev#), but frankly I'm just trying to learn and having to think about git slows me down.

Hopefully I can wrap my head around pipelining for JZJCoreC.

## Cycle counts for instructions

Note: raw cycle counts do not include initial instruction fetch clock cycle which adds 1 clock delay to each instruction

| Instruction | Raw Cycle Count | Cycles + Inst Fetch |
|:------------|:-----------------------|:--------------------|
|-Base Spec(I)-|
| lui | 1 | 2 |
| auipc | 1 | 2 |
| jal | 1 | 2 |
| jalr | 1 | 2 |
| beq | 1 | 2 |
| bne | 1 | 2 |
| blt | 1 | 2 |
| bge | 1 | 2 |
| bltu | 1 | 2 |
| bgeu | 1 | 2 |
| lb | 2 | 3 |
| lh | 2 | 3 |
| lw | 2 | 3 |
| lbu | 2 | 3 |
| lhu | 2 | 3 |
| sb | 2 | 3 |
| sh | 2 | 3 |
| sw | 1 | 2 |
| addi | 1 | 2 |
| slti | 1 | 2 |
| sltiu | 1 | 2 |
| xori | 1 | 2 |
| ori | 1 | 2 |
| andi | 1 | 2 |
| slli | 1 | 2 |
| srli | 1 | 2 |
| srai | 1 | 2 |
| add | 1 | 2 |
| sub | 1 | 2 |
| sll | 1 | 2 |
| slt | 1 | 2 |
| sltu | 1 | 2 |
| xor | 1 | 2 |
| srl | 1 | 2 |
| sra | 1 | 2 |
| or | 1 | 2 |
| and | 1 | 2 |
| fence | 1 | 2 |
| ecall | 1 | 2 |
| ebreak | 1 | 2 |
|-Zifencei-|
| fence.i | 1 | 2 |

## Memory Map

Note: addresses are inclusive, bounds not specified are not addressed to anything, and execution starts at 0x00000000

| Bytewise Address (whole word) | Physical Word-wise Address | Function |
|:------------------------------|:---------------------------|:---------|
|0x00000000 to 0x00000003|0x00000000|RAM Start|
|0x0000FFFC to 0x0000FFFF|0x00003fff|Ram End|
|0xFFFFFFE0 to 0xFFFFFFE3|0x3FFFFFF8|Memory Mapped IO Registers Start|
|0xFFFFFFFC to 0xFFFFFFFF|0x3FFFFFFF|Memory Mapped IO Registers End|
