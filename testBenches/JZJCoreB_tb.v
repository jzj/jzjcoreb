`timescale 1ns/1ps
module JZJCoreB_tb;

reg clock = 1'b0;

JZJCoreB #(.INITIAL_MEM_CONTENTS("../src/memFiles/adding2.mem")) coreTest (.clock(clock), .reset(0));

always
begin
    #10;
    clock = ~clock;
end

initial
begin
    $dumpfile("/tmp/JZJCoreB.vcd");
    $dumpvars(0,JZJCoreB_tb);
    #1000 $finish;
end

endmodule
