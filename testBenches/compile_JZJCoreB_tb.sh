cd ../src
iverilog -o /tmp/JZJCoreB.vvp ../testBenches/JZJCoreB_tb.v JZJCoreB/ALU.v JZJCoreB/ControlLogic.v JZJCoreB/InstructionDecoder.v JZJCoreB/JZJCoreB.v JZJCoreB/PCALU.v JZJCoreB/ProgramCounter.v JZJCoreB/RegisterFile.v JZJCoreB/ValueFormer.v JZJCoreB/Memory/MemoryBackend.v JZJCoreB/Memory/MemoryController.v JZJCoreB/Memory/sync_ram.v JZJCoreB/Memory/MemoryMappedIOManager.v JZJCoreB/Memory/RAMWrapper.v
vvp /tmp/JZJCoreB.vvp
gtkwave /tmp/JZJCoreB.vcd
